﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.IO;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext()
        {
            
        }         

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }     
        
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var limitString = 50;            
            
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);

           
            modelBuilder.Entity<Employee>().Property(b => b.FirstName).HasMaxLength(limitString);
            modelBuilder.Entity<Employee>().Property(b => b.LastName).HasMaxLength(limitString);
            modelBuilder.Entity<Employee>().Property(b => b.Email).HasMaxLength(limitString);

            modelBuilder.Entity<Role>().Property(b => b.Name).HasMaxLength(limitString);
            modelBuilder.Entity<Role>().Property(b => b.Description).HasMaxLength(limitString);

            modelBuilder.Entity<Customer>().Property(b => b.FirstName).HasMaxLength(limitString);
            modelBuilder.Entity<Customer>().Property(b => b.LastName).HasMaxLength(limitString);
            modelBuilder.Entity<Customer>().Property(b => b.Email).HasMaxLength(limitString);

            modelBuilder.Entity<Preference>().Property(b => b.Name).HasMaxLength(limitString);

            modelBuilder.Entity<PromoCode>().Property(b => b.Code).HasMaxLength(limitString);
            modelBuilder.Entity<PromoCode>().Property(b => b.ServiceInfo).HasMaxLength(limitString);
            modelBuilder.Entity<PromoCode>().Property(b => b.PartnerName).HasMaxLength(limitString);

        }
    }
}
